# plants dev blog

I built this tool to make it easier to see updates from the Plants vs Vegans project.

Trello is the project management that I decided to use to track and plan the development of the project. Now this board is public and can be found here

https://trello.com/b/kgyiDnZv/plants-vs-vegans

It is possible to see the updates of the board by frequently looking at it but if people want to see the recent updates just the cards don't tell the full picture, especially for people that aren't following the project closely

So I decided to build a development blog but not one that I would have to update manually - as I was already documenting that progress of the project and my thoughts on the trello board.

This tool is designed to use the trello api to get all the comments from the board, extract the key data that we want and then render the comments sorted by date as like a newsfeed on a webpage.

It has a NodeJS backend and react frontend


