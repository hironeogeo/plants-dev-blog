import express from "express";
import cors from "cors";
import fetch from "node-fetch";

const trelloApiKey = "075c048157e325b36d21b2e2c4fa00ba";
const trelloToken =
  "ATTA17ce2e922b0fa1fab5402d98882c9410a8f87f4bf57a18e603840676f0a20516D1E3C0CB";
const trelloPlantsBoard = "5dbe908c513eaf0132f88f9e";

const app = express();
app.use(cors());

// Generic function to make a call and get the response from the trelloApi
async function callTrelloApi(fetchString, debug = false) {
  if (debug) {
    console.log(fetchString, {
      method: "GET",
      headers: {
        Accept: "application/json",
      },
    });
  }

  let response = await fetch(fetchString);

  if (debug) {
    console.log(response);
    console.log(response.json());
  }
  return response.json();
}

function extractTrelloListIds(trelloListJson) {
  var trelloListIds = [];

  for (const obj of trelloListJson) {
    trelloListIds.push(obj.id);
  }
  return trelloListIds;
}

// helper function to extract trello card comment data
function extractTrelloCardCommentData(trelloCardCommentJson) {
  var trelloCardCommentObjs = [];
  for (const obj of trelloCardCommentJson) {
    var commentObj = new Object();
    commentObj.Id = obj.id;
    commentObj.date = new Date(obj.date).toUTCString();
    commentObj.cardName = obj.data.card.name;
    commentObj.listName = obj.data.list.name;
    commentObj.author = obj.memberCreator.username;
    commentObj.comment = obj.data.text;
    commentObj.cardLink =
      "trello.com/c/" +
      obj.data.card.shortLink +
      "/" +
      obj.data.card.idShort +
      "-" +
      obj.data.card.name;

    trelloCardCommentObjs.push(commentObj);
  }
  return trelloCardCommentObjs;
}

// helper function to extract trello card ids
function extractTrelloCardIds(trelloCardJson) {
  //console.log(trelloCardJson)
  var trelloCardIds = [];
  for (const obj of trelloCardJson) {
    trelloCardIds.push(obj.id);
  }
  return trelloCardIds;
}

const fetchAllTrelloCardIds = async () => {
  const getAllTrelloCardsFetchString2 =
    "https://api.trello.com/1/boards/" +
    trelloPlantsBoard +
    "/cards/all?key=" +
    trelloApiKey +
    "&token=" +
    trelloToken;
  const cards = await callTrelloApi(getAllTrelloCardsFetchString2);
  const cardIds = extractTrelloCardIds(cards);
  return cardIds;
};

const fetchGetTrelloCardComments = async (cardId) => {
  const getTrelloCardCommentsFetchString =
    "https://api.trello.com/1/cards/" +
    cardId +
    "/actions?filter=commentCard&key=" +
    trelloApiKey +
    "&token=" +
    trelloToken;
  const cardComments = await callTrelloApi(getTrelloCardCommentsFetchString);
  const extractedComments = extractTrelloCardCommentData(cardComments);
  return extractedComments;
};

app.get("/getAllTrelloComments", async (req, res) => {
  var cardComments = [];
  const cardIds = await fetchAllTrelloCardIds();

  cardIds.forEach(async function (cardId) {
    const extractedCardComments = await fetchGetTrelloCardComments(cardId);
    //console.log(extractedCardComments);
    cardComments.push(extractedCardComments);
  });

  var comments = [];
  var onComplete = function () {
    res.writeHead(200);
    res.end(comments);
  };
  const getAllTrelloCardsFetchString2 =
    "https://api.trello.com/1/boards/" +
    trelloPlantsBoard +
    "/cards/all?key=" +
    trelloApiKey +
    "&token=" +
    trelloToken;
  callTrelloApi(getAllTrelloCardsFetchString2)
    .then((trelloCards) => {
      console.log("hellloooooooo");
      // console.log(trelloCards);
      const cardsIds = extractTrelloCardIds(trelloCards);
      var tasksToGo = cardsIds.length;

      if (tasksToGo === 0) {
        onComplete();
      } else {
        // there is at least one element so the callback will be called
        cardsIds.forEach(function (cardId) {
          comments.push(1);
          const getTrelloCardCommentsFetchString =
            "https://api.trello.com/1/cards/" +
            cardId +
            "/actions?filter=commentCard&key=" +
            trelloApiKey +
            "&token=" +
            trelloToken;
          callTrelloApi(getTrelloCardCommentsFetchString).then(
            (trelloCardComments) => {
              if (trelloCardComments.length > 0) {
                // comments.push(extractTrelloCardCommentData(trelloCardComments))
                //var extractedComments = extractTrelloCardCommentData(trelloCardComments);
                //console.log(comments);

                comments.push(extractTrelloCardCommentData(trelloCardComments));
                //console.log(comments);
                //console.log(extractedComments);
                //comments = extractedComments;
                //console.log(comments);
                //comments.concat(comments,extractTrelloCardCommentData(trelloCardComments))
                //comments.concat(comments,extractedComments);
                if (--tasksToGo === 0) {
                  // no tasks left, good to go
                  //console.log(comments);
                  onComplete();
                }
              }
            }
          );
          //console.log(comments);
        });
      }

      //console.log(cardsIds.length);
      //console.log(cardsIds);
      //console.log(comments)
      //console.log(cardComments.length);

      var filtered = cardComments.filter(function (el) {
        return el.length != 0;
      })

      var flatComments = [];
      filtered.forEach(function (element){
        element.forEach(function (comment) {
          flatComments.push(comment);
        })
      })

      flatComments.sort(function(a,b){
        return new Date(b.date) - new Date(a.date);
      });
      

      return res.json(flatComments);
    })
    .catch((err) => {
      console.log("error with get all trello card ids api");
      console.log(err);
    });
});

app.listen(8081, () => {
  console.log("connected to backend!");
});

// we need to get the comments from trello
// so we need to get all the cards, and then for each card we need to get the comments
// we'll use react to create objs out of them and then render them on the screen

app.get("/getTrelloCardComments/:cardId", (req, res) => {
  console.log("getTrelloCardComments api");
  const getTrelloCardCommentsFetchString =
    "https://api.trello.com/1/cards/" +
    req.params.cardId +
    "/actions?filter=commentCard&key=" +
    trelloApiKey +
    "&token=" +
    trelloToken;
  callTrelloApi(getTrelloCardCommentsFetchString)
    .then((trelloCardComments) => {
      const cardComments = extractTrelloCardCommentData(trelloCardComments);
      console.log(cardComments.length);
      console.log(cardComments);
      return res.json(cardComments);
    })
    .catch((err) => {
      console.log("error with get trello card comments api");
      console.log(err);
    });
});

app.get("/getAllTrelloCardIds", (req, res) => {
  //console.log("getAllTrelloCardIds api");
  const getAllTrelloCardsFetchString =
    "https://api.trello.com/1/boards/" +
    trelloPlantsBoard +
    "/cards/all?key=" +
    trelloApiKey +
    "&token=" +
    trelloToken;
  callTrelloApi(getAllTrelloCardsFetchString)
    .then((trelloCards) => {
      const cardsIds = extractTrelloCardIds(trelloCards);
      //console.log(cardsIds.length);
      //console.log(cardsIds);
      return res.json(cardsIds);
    })
    .catch((err) => {
      console.log("error with get all trello card ids api");
      console.log(err);
    });
});
