import { useEffect, useState } from 'react'
import './App.css'
import { TrelloComment } from '../TrelloComment'

function App() {

  const [requestSent, setRequestSent] = useState();
  const [waitingForRequest, setWaitingForRequest] = useState();
  

  const [trelloComments, setTrelloComments] = useState([]);

  useEffect(() => {
    console.log("calling our use effect")
    setRequestSent(true);
    if (!requestSent && !waitingForRequest) {
      setWaitingForRequest(true);
      fetch("http://localhost:8081/getAllTrelloComments")
      .then(response => response.json())
      .then(data => setTrelloComments(data))
      .catch(err => console.log(err));

    }

  }, [requestSent, waitingForRequest])

  return (

    
    <div> 
      <h1> Plants Vs Vegans Dev Blog</h1>
      {trelloComments.map((element) => { 
        return (
      <TrelloComment 
      id={element.Id}
      date={element.date}
      cardName={element.cardName}
      listName={element.listName}
      author={element.author}
      comment={element.comment}
      link={element.cardLink}
      > /</TrelloComment>
        )
      })}
    </div>
  )
}

export default App
