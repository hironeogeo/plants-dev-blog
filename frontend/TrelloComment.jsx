import * as React from 'react';
import Box from '@mui/material/Box';

export const TrelloComment = ({id, date, cardName, listName, author, comment, link}) => {

    return (
    
        <div>
            <Box 
            sx={{
                width: 800,
                height: 400,
                borderColor: 'blue',
                borderStyle: 'solid',
                textAlign: 'left',
                paddingLeft: '15px',
                paddingBottom: '50px'

            }}>
                {/* <p> 
                    Id: {id}
                </p> */}
                <p>
                    Date: {date}
                </p>
                <p>
                    Card: {cardName}
                </p>
                <p>
                    List: {listName}
                </p>
                <p>
                    Author: {author}
                </p>
                <p>
                    Comment: {comment}
                </p>
                <p>
                    Link: {link}
                </p>

            </Box>
        </div>
    );

}
